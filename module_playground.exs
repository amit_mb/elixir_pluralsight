defmodule ModulePlayground do
import IO,only: [puts: 1]
import Kernel, except: [inspect: 1]
#This is similar to saying import { puts } from 'io', with an additional arity also required here

#using alias
    alias ModulePlayground.Misc.Math, as: MyMath
    require Integer
    def say_here do
        inspect "Hello Elixir"
    end

    #access as ModulePlayground.inspect
    def inspect(string) do
        puts "Starting to Inspect"
        puts string
        puts "Ending Inspect"
    end

    #access as ModulePlayground.sum1and2
    def sum3and4 do
        MyMath.sum(3,4)
    end

    def isnumeven(num) do
        IO.puts Integer.is_even(num)
    end
end

#SUMMARY
#`import` will bring in functions from other modules. You can use :only and :except to import specific functions.

#`alias` will bring in whole module. It ll make those imported module names shorter. reduce typing, also
# can rename a module in your module

#require will allow the usage of macros in your modules

