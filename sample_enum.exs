defmodule Sample.Enum do #Accessed as Sample.Enum

    def first(list) when length(list) == 0 do #performs the check length(list)==0. if yes, executes
                                             #This do block, else checks for next def with same name
        nil
    end

    def first([head | _]) do
        head
    end

    #Advantages of pattern matching
    def calculateBillAmt(quantity, {_, _, price}) do
        quantity * price
    end

    #Vs

    def calculateBillAmount(quantity, book) do
        quantity * elem(book, 2)
    end
end

#Sample.Enum.first([1,2,3]) -> 1
#Sample.Enum.first([]) -> :nil/nil