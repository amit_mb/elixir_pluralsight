defmodule ListModule do
    def addtoList(list , value \\ []) do # \\ is used for defining default parameters
        trace(value)
        [value | list]
    end
    #ListModule.addtoList([1,2,3], 4)
    #[4, 1, 2, 3]

    defp trace(string) do #private function
        IO.puts "The value to be added to list is #{string}"
    end

    def doubleList(list) do
        Enum.map(list, fn(val) -> val * 2 end)
    end
    #ListModule.doubleList([1,2,3])
    #[1,4,6]

    def squareList(list) do
        Enum.map(list, fn(val) -> val * val end)
    end
    #ListModule.squareList([1,2,3,4])
    #[1, 4, 9, 16]
end