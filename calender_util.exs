defmodule Calender.Util do
    #first check for this. if false, move on to next check
    def is_leapyear(year) when rem(year, 400) == 0 do
        true
    end

    def is_leapyear(year) when rem(year,100) == 0 do
        false
    end

    def is_leapyear(year) when rem(year, 4) == 0 do
        true
    end

    def is_leapyear(year) do
        false
    end
end